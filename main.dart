import 'package:flutter/material.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  OverlayState? overlayState; /// 여기에다 Entry 를 다 담아놓음
  OverlayEntry? overlayEntry;
  OverlayEntry? overlayEntry1;
  OverlayEntry? overlayEntry2;
  OverlayEntry? overlayEntry3;
  OverlayEntry? overlayEntry4;
  OverlayEntry? overlayEntry5;
  bool showOverlay = false;
  bool showOverlay1 = false;
  bool showOverlay2 = false;
  bool showOverlay3 = false;
  bool showOverlay4 = false;
  bool showOverlay5 = false;
  final textButtonKey = GlobalKey();
  final textButtonKey1 = GlobalKey();
  final textButtonKey2 = GlobalKey();
  final textButtonKey3 = GlobalKey();
  final textButtonKey4 = GlobalKey();
  final textButtonKey5 = GlobalKey();
  List<GlobalKey> textButtonKeys = [GlobalKey(), GlobalKey(), GlobalKey(), GlobalKey(), GlobalKey(), GlobalKey()];

  int index = 0;
  List<Widget> widgets = [
    Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Greetings'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('About us'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('History'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Organization'),
                  onPressed: (){
            print('Clicked');
            },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Mission & Vision & Ci'),
                  onPressed: (){
            print('Clicked');
            },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Social Contribution'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
        ],
      ),
    ),
    Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Business field'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('R&D'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Quality'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),

        ],
      ),
    ),

    Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('EPI'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('CVD'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('LED'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('part'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('5G'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('SIC'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
        ],
      ),
    ),
    Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('HR system'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Talent'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),
        ],
      ),
    ),
    Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 50,
            width: 100,
            child: TextButton(
              child: Text('Contact'),
              onPressed: (){
                print('Clicked');
              },
            ),
          ),

        ],
      ),
    ),
    Container(
      height: 300,
      width: 100,
      color: Colors.pink,
      child: TextButton(child: Text('4'),
        onPressed: (){
          print('Clicked');
        },
      ),
    ),
    Container(
      height: 300,
      width: 100,
      color: Colors.cyan,
      child: TextButton(child: Text('5'),
        onPressed: (){
          print('Clicked');
        },
      ),
    ),
    Container(
      height: 300,
      width: 100,
      color: Colors.teal,
      child: TextButton(child: Text('6'),
        onPressed: (){
          print('Clicked');
        },
      ),
    ),
    Container(
      height: 300,
      width: 100,
      color: Colors.indigoAccent,
      child: TextButton(child: Text('7'),
        onPressed: (){
          print('Clicked');
        },
      ),
    ),
    Container(
      height: 300,
      width: 100,
      color: Colors.amber,
      child: TextButton(child: Text('8'),
        onPressed: (){
          print('Clicked');
        },
      ),
    ),
    Container(
      height: 300,
      width: 100,
      color: Colors.deepOrangeAccent,
      child: TextButton(child: Text('9'),
        onPressed: (){
          print('Clicked');
        },
      ),
    ),
  ];
  final layerLink = LayerLink();
  final textButtonFocusNode = FocusNode();
  final textButtonFocusNode1 = FocusNode();
  final textButtonFocusNode2 = FocusNode();
  final textButtonFocusNode3 = FocusNode();
  final textButtonFocusNode4 = FocusNode();
  final textButtonFocusNode5 = FocusNode();

  void _showOverlay(BuildContext context, int index) async {
    overlayState = Overlay.of(context)!;
    RenderBox box = textButtonKeys[index].currentContext?.findRenderObject() as RenderBox;
    Offset position = box.localToGlobal(Offset.zero);
    double x = position.dx;
    double y = position.dy;
    print(index);
    overlayEntry = OverlayEntry(
        maintainState: false,
        builder: (context) {
          return Positioned(
            left:x,
            top: y + 30,
            child: TextButton(
              onPressed: () {},
              onHover: (val) {
                if (val && showOverlay) {
                  switch (index) {
                    case 0:
                      textButtonFocusNode.requestFocus();
                      break;
                    case 1:
                      textButtonFocusNode1.requestFocus();
                      break;
                    case 2:
                      textButtonFocusNode2.requestFocus();
                      break;
                    case 3:
                      textButtonFocusNode3.requestFocus();
                      break;
                    case 4:
                      textButtonFocusNode4.requestFocus();
                      break;
                    case 5:
                      textButtonFocusNode5.requestFocus();
                      break;
                  }

                } else {
                  switch (index) {
                    case 0:
                      textButtonFocusNode.unfocus();
                      break;
                    case 1:
                      textButtonFocusNode1.unfocus();
                      break;
                    case 2:
                      textButtonFocusNode2.unfocus();
                      break;
                    case 3:
                      textButtonFocusNode3.unfocus();
                      break;
                    case 4:
                      textButtonFocusNode4.unfocus();
                      break;
                    case 5:
                      textButtonFocusNode5.unfocus();
                      break;
                  }
                }
              },
              child: widgets[index],
            ),
          );
        });

    overlayState!.insertAll([overlayEntry!,]);
  }

  void removeOverlay() {
    overlayEntry!.remove();
    overlayEntry2!.remove();
    overlayEntry3!.remove();
    overlayEntry4!.remove();
    overlayEntry5!.remove();
  }

  @override
  void initState() {
    super.initState();
    textButtonFocusNode.addListener(() {
      if (textButtonFocusNode.hasFocus) {
        _showOverlay(context, 0);
      } else {
        removeOverlay();
      }
    });
    textButtonFocusNode1.addListener(() {
      if (textButtonFocusNode1.hasFocus) {
        _showOverlay(context, 1);
      } else {
        removeOverlay();
      }
    });
    textButtonFocusNode2.addListener(() {
      if (textButtonFocusNode2.hasFocus) {
        _showOverlay(context, 2);
      } else {
        removeOverlay();
      }
    });
    textButtonFocusNode3.addListener(() {
      if (textButtonFocusNode3.hasFocus) {
        _showOverlay(context, 3);
      } else {
        removeOverlay();
      }
    });
    textButtonFocusNode4.addListener(() {
      if (textButtonFocusNode4.hasFocus) {
        _showOverlay(context, 4);
      } else {
        removeOverlay();
      }
    });
    textButtonFocusNode5.addListener(() {
      if (textButtonFocusNode5.hasFocus) {
        _showOverlay(context, 5);
      } else {
        removeOverlay();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,

        actions: [

          TextButton(child: Text("lwerjalkef"), onPressed: (){},)
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          border: Border.all()
        ),
        height: 100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              key: textButtonKeys[0],
              focusNode: textButtonFocusNode,
              onHover: (val) {
                if (val) {
                  textButtonFocusNode.requestFocus();
                  showOverlay = true;
                }
                else {
                  textButtonFocusNode.unfocus();
                }
              },
              onPressed: () {},
              child: Container(child: const Text('Company')),
            ),
            TextButton(
              key: textButtonKeys[1],
              focusNode: textButtonFocusNode1,
              onHover: (val) {
                if (val) {
                  textButtonFocusNode1.requestFocus();
                  showOverlay1 = true;
                }
              },
              onPressed: () {},
              child: const Text('Business'),
            ),
            TextButton(
              key: textButtonKeys[2],
              focusNode: textButtonFocusNode2,
              onHover: (val) {
                if (val) {
                  textButtonFocusNode2.requestFocus();
                  showOverlay2 = true;
                }
              },
              onPressed: () {},
              child: const Text('Product'),
            ),
            TextButton(
              key: textButtonKeys[3],
              focusNode: textButtonFocusNode3,
              onHover: (val) {
                if (val) {
                  textButtonFocusNode3.requestFocus();
                  showOverlay3 = true;
                }
              },
              onPressed: () {},
              child: const Text('HR'),
            ),
            TextButton(
              key: textButtonKeys[4],
              focusNode: textButtonFocusNode4,
              onHover: (val) {
                if (val) {
                  textButtonFocusNode4.requestFocus();
                  showOverlay4 = true;
                }
              },
              onPressed: () {},
              child: const Text('C/S'),
            ),
          ],
        ),
      ),


    ); }
}